# docker-builds
Varirous builds for custom docker containers

## [ubuntu-android](ubuntu-android)
To use this contaner first build it with

```
docker build -t ubuntu-android ubuntu-android/
```
To start a new container run
```
docker run \
    --name ubuntu-android \
    --hostname android-build \
    -it --privileged \
    --volume /srv/ubuntu-android:/home \
    --volume /dev/bus/usb:/dev/bus/usb \
    ubuntu-android
```

## [flarum](flarum)
To use this contaner first build it with

```
docker build -t flarum flarum/
```
To start a new container run
```
docker run -d \
    --name flarum \
    --hostname flarum \
    --publish 8082:8082 \
    --volume /srv/flarum:/srv/flarum \
    flarum
```

## [tiddlywiki](tiddlywiki)
To use this contaner first build it with

```
docker build -t tiddlywiki tiddlywiki/
```
To start a new container run
```
docker run -d \
    --name tiddlywiki \
    --hostname tiddlywiki \
    --publish 8080:8080 \
    --volume /srv/tiddlywiki:/srv/tiddlywiki \
    tiddlywiki
```
